import * as fs from "fs";
import {FizzBuzz} from "./fbclass";

// Main program
const no_args: number = process.argv.length;
if (no_args < 3) {
	console.log("Missing arguments");
} else {
    let fb: FizzBuzz;
    const lines: string[] = fs.readFileSync(process.argv[2], 'utf8').split("\n");
    if (no_args < 4) {
        fb = new FizzBuzz();
    } else if (no_args < 5) {
        fb = new FizzBuzz(process.argv[3]);
    } else {
        fb = new FizzBuzz(process.argv[3], process.argv[4]);
    }
    for (const line of lines) {
        let pair: string[] = line.split(",");
        fb.print(parseInt(pair[0]), parseInt(pair[1]));
    }
}
