// FizzBuzz object
class FizzBuzz {
    x: string;
    y: string;

    constructor(x: string = "Fizz", y:string = "Buzz") {
        this.x = x;
        this.y = y;
    }

    // Main FizzBuzz operation
    print(start: number, end: number) {
        for (let i: number = start; i <= end; i++) {
            if (i % 3 == 0) {
                if (i % 5 == 0) {
                    process.stdout.write(this.x + this.y);
                } else {
                    process.stdout.write(this.x);
                }
            } else {
                if (i % 5 == 0) {
                    process.stdout.write(this.y);
                } else {
                    process.stdout.write("" + i);
                }
            }
            if (i !== end) {
                process.stdout.write(" ");
            }
        }
        process.stdout.write("\n");
    }
}

export {FizzBuzz}