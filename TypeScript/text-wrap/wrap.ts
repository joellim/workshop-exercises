import * as fs from "fs";

// Helper function to split string into chunks based on max word length
function wrap_text(text: string, num: number): Array<string> {
	let lines: Array<string> = [];
	let words: Array<string> = text.split(" ");
	const no_words = words.length;
	let ptr = 0;
	while (ptr < no_words) {
		if (ptr + num < no_words) {
			lines.push(words.slice(ptr, ptr + num).join(' '));
			ptr += num;
		} else {
			lines.push(words.slice(ptr).join(' '));
			break;
		}
	}
	return lines;
}	

// Helper function to write lines into a destination file
function write_lines(lines: Array<string>, dest: string): void {
	fs.writeFileSync(dest, lines.join('\n'));
}

// Main program
if (process.argv.length < 4) {
	console.log("Missing arguments");
} else {
	const src: string = process.argv[2];
	const text: string = fs.readFileSync(src, 'utf8');
	const num: number = parseInt(process.argv[3]);
	write_lines(wrap_text(text, num), src);
}

