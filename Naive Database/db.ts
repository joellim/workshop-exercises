import * as fs from "fs";

// Helper functions to load and save persistent data
function load_db(): object {
    return JSON.parse(fs.readFileSync("db.json", "utf8"));
}

function save_db(db: object): void {
    fs.writeFileSync("db.json", JSON.stringify(db))
}

// Helper functions to support CRUD operations
function save(db: object, key: string, value: string): void{
    db[key] = value;
}

function load(db: object, key:string): string {
    if (key in db) {
        return db[key];
    } else {
        return "NULL";
    }
}

function del(db: object, key:string): void {
    if (key in db) {
        delete db[key];
    }
}

// Main
const no_args = process.argv.length;
if (no_args < 3) {
    console.log("Missing arguments");
} else {
    let db = load_db();
    const cmd: string = process.argv[2];
    if (cmd == "save") {
        if (no_args >= 5) {
            save(db, process.argv[3], process.argv.slice(4).join(" "));
        } else {
            console.log("Missing arguments");
        }
    } else if (cmd == "load") {
        if (no_args >= 4) {
            console.log(load(db, process.argv[3]));
        } else {
            console.log("Missing arguments");
        }
    } else if (cmd == "delete") {
        if (no_args >= 4) {
            del(db, process.argv[3]);
        } else {
            console.log("Missing arguments");
        }
    }
    save_db(db);
}

