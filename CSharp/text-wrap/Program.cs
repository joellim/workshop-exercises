﻿using System;
using System.IO;
using System.Collections.Generic;

namespace text_wrap
{
    class Program
    {
        // Helper function to split string into chunks based on max word length
        static List<string> wrap_text(string text, int num) {
            List<string> lines = new List<string>();
            string[] words = text.Split(" ");
            int no_words = words.Length;
            int ptr = 0;
            while (ptr < no_words) {
                if (ptr + num < no_words) {
                    lines.Add(string.Join(" ", words[ptr..(ptr+num)]));
                    ptr += num;
                } else {
                    lines.Add(string.Join(" ", words[ptr..^0]));
                    break;
                }
            }
            return lines;
        }
        // Helper function to write lines into destination file
        static void write_lines(List<string> lines, string dest) {
            File.WriteAllText(dest, string.Join("\n", lines));
        }
        static void Main(string[] args)
        {
            if (args.Length < 2) {
                Console.WriteLine("Missing args");
            } else {
                string src = args[0];
                string text = File.ReadAllText(src);
                write_lines(wrap_text(text, Int32.Parse(args[1])), src);
            }
        }


    }
}
