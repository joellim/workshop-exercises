﻿using System;
using System.IO;

namespace fizzbuzz {
    class FizzBuzz {
        string x;
        string y;
        public FizzBuzz(string xx = "Fizz", string yy = "Buzz") {
            x = xx;
            y = yy;
        }

        // Main FizzBuzz operation
        public void print(int start, int end) {
            for (int i = start; i <= end; i++) {
                if (i % 3 == 0) {
                    if (i % 5 == 0) {
                        Console.Write(x + y);
                    } else {
                        Console.Write(x);
                    }
                } else {
                    if (i % 5 == 0) {
                        Console.Write(y);
                    } else {
                        Console.Write(i);
                    }
                }
                if (i != end) {
                    Console.Write(" ");
                }
            }
            Console.Write("\n");
        }

    }
    class Program {
        static void Main(string[] args) {
            int no_args = args.Length;
            if (no_args < 1) {
                Console.WriteLine("Missing args");
            } else {
                FizzBuzz fb;
                string[] lines = File.ReadAllText(args[0]).Split("\n");
                if (no_args < 2) {
                    fb = new FizzBuzz();
                } else if (no_args < 3) {
                    fb = new FizzBuzz(args[1]);
                } else {
                    fb = new FizzBuzz(args[1], args[2]);
                }
                foreach (string line in lines) {
                    string[] pair = line.Split(",");
                    fb.print(Int32.Parse(pair[0]), Int32.Parse(pair[1]));
                }
            }
        }
    }
}